/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

namespace ActsTrk::detail {

  template <typename state_t>
  inline void MeasurementCalibratorBase::setProjector(xAOD::UncalibMeasType measType,
						      Acts::SurfaceBounds::BoundsType boundType,
						      state_t &trackState ) const
  {
    
    switch (measType) {
    case xAOD::UncalibMeasType::StripClusterType: {
      const std::size_t projector_idx  = boundType == Acts::SurfaceBounds::eAnnulus;
      trackState.setProjectorSubspaceIndices(s_stripSubspaceIndices[projector_idx]);
      break;
    }
    case xAOD::UncalibMeasType::PixelClusterType: {
      trackState.setProjectorSubspaceIndices(s_pixelSubspaceIndices);
      break;
    }
    case xAOD::UncalibMeasType::HGTDClusterType: {
      trackState.setProjectorSubspaceIndices(s_hgtdSubspaceIndices);
      break;
       }
    default:
      throw std::domain_error("Can only handle measurement type pixel or strip");
    }
    
  }
  
  template <std::size_t Dim,
	    typename pos_t,
	    typename cov_t,
	    typename state_t>
  inline void MeasurementCalibratorBase::setState(xAOD::UncalibMeasType measType,
						  const pos_t& locpos,
						  const cov_t& cov,
						  Acts::SurfaceBounds::BoundsType boundType,
						  state_t &trackState) const
  {
    trackState.allocateCalibrated(Dim);
    setProjector(measType, boundType, trackState);
    trackState.template calibrated<Dim>() = locpos.template cast<double>();
    trackState.template calibratedCovariance<Dim>() = cov.template cast<double>();
  }
  
  template <class measurement_t,
	    typename trajectory_t>
  inline void MeasurementCalibratorBase::setStateFromMeasurement(const measurement_t &measurement,
								 Acts::SurfaceBounds::BoundsType bound_type,
								 typename Acts::MultiTrajectory<trajectory_t>::TrackStateProxy &trackState ) const
  {

    switch (measurement.type()) {
    case (xAOD::UncalibMeasType::StripClusterType): {
      setState<1>(measurement.type(),
		  measurement.template localPosition<1>(),
		  measurement.template localCovariance<1>().template topLeftCorner<1, 1>(),
		  bound_type,
		  trackState);
      break;
    }
    case (xAOD::UncalibMeasType::PixelClusterType): {
      setState<2>(measurement.type(),
		  measurement.template localPosition<2>(),
		  measurement.template localCovariance<2>().template topLeftCorner<2, 2>(),
		  bound_type,
		  trackState);
      break;
    }
    case (xAOD::UncalibMeasType::HGTDClusterType): {
     setState<3>(measurement.type(),
                 measurement.template localPosition<3>(),
                 measurement.template localCovariance<3>().template topLeftCorner<3, 3>(),
                 bound_type,
                 trackState);
      break;
    }
    default:
      throw std::domain_error("Can only handle measurement type pixel or strip");
    }
  }
  
} // namespace ActsTrk::detail

