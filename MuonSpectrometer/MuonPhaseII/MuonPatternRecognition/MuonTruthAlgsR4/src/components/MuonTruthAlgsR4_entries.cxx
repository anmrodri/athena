

/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/
#include "../TruthSegmentMaker.h"
#include "../PrepDataToSimHitAssocAlg.h"
#include "../PrdMultiTruthMaker.h"
#include "../TruthSegToTruthPartAssocAlg.h"
#include "../TrackToTruthPartAssocAlg.h"
DECLARE_COMPONENT(MuonR4::TruthSegmentMaker)
DECLARE_COMPONENT(MuonR4::PrepDataToSimHitAssocAlg)
DECLARE_COMPONENT(MuonR4::PrdMultiTruthMaker)
DECLARE_COMPONENT(MuonR4::TruthSegToTruthPartAssocAlg)
DECLARE_COMPONENT(MuonR4::TrackToTruthPartAssocAlg)